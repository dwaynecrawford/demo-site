# Test Site

> Test site setup for demo using contentful & nuxtjs

## Demo

[Demo here](https://mssv-test-site.herokuapp.com/)

> Note: This repo isn't styled or designed visually. Its main purpose is to display components setup and page structure, and how the cms api works with reusable components.

## Build Setup

```bash
# install dependencies
$ npm run install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```
