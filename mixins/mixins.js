export default {
  head() {
    return {
      title: `${this.title} | ${process.env.SITE_TITLE}`
    }
  },
  data() {
    return {
      title: ''
    }
  },
  mounted() {
    this.$pageView({
      title: this.title
    })
  }
}
