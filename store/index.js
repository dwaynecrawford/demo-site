// Imports contentful for navigation connection
import { createClient } from '~/plugins/contentful.js'
const client = createClient()

// exports state for use in other pages and components
export const state = () => ({
  navigationItems: []
})

export const mutations = {
  setNavigationItems(state, payload) {
    state.navigationItems = [...payload]
  }
}

export const actions = {
  async nuxtServerInit({ commit }) {
    let [primaryNav] = await Promise.all([
      client.getEntries({
        'sys.id': '1tVNzhdmDo2tlO1ggPWjfi',
        include: 3
      })
    ])

    commit('setNavigationItems', primaryNav.items[0].fields.navigationItems)
  }
}
