import { documentToHtmlString } from '@contentful/rich-text-html-renderer'

function pageAdapter(obj) {
  function heroAdapter(obj) {
    if (!obj || !obj.fields) {
      return {
        img: '',
        lead: '',
        textPosition: 'center'
      }
    }

    let hero = {
      img: obj.fields.image ? obj.fields.image.fields.file.url : '',
      subheader: obj.fields.subheader || '',
      lead: obj.fields.lead || '',
      subLead: obj.fields.subLead || '',
      title: obj.fields.title || ''
    }
    return hero
  }

  let body = {
    content: [],
    data: {},
    nodeType: 'document'
  }

  let page = {
    title: obj.title || '',
    image: obj.image || '',
    description: obj.pageDescription || '',
    keywords: obj.keywords || [],
    body: obj.body || body,
    renderedHTML: obj.body ? documentToHtmlString(obj.body, {}) : '',
    spaRouteChange: process.client,
    hero: obj.hero ? heroAdapter(obj.hero) : {},
    index: obj.index || false,
    socialImage: obj.socialImage ? obj.socialImage.fields.file.url : '',
    heroImage: obj.heroImage ? obj.heroImage.fields.file.url : ''
  }
  return page
}

export { pageAdapter }
