import { documentToHtmlString } from '@contentful/rich-text-html-renderer'
import { BLOCKS } from '@contentful/rich-text-types'
import { pageAdapter } from '@/utils/pageAdapter/adapters/page'

const options = {
  renderNode: {
    [BLOCKS.EMBEDDED_ENTRY]: (node) => {
      // console.log(node)
      return `<img class="float-right" src="${node.value}">`
    }
  }
}

function processPageFields(obj) {
  return {
    ...obj,
    ...{
      title: obj.title || '',
      image: obj.image || '',
      body: obj.body || '',
      renderedHTML: obj.body ? documentToHtmlString(obj.body, options) : '',
      components: obj.components || [],
      spaRouteChange: process.client,
      hero: obj.hero || '',
      index: obj.index || false
    }
  }
}

function renderBody(doc) {
  return documentToHtmlString(doc, options)
}

export { processPageFields, renderBody, pageAdapter }
